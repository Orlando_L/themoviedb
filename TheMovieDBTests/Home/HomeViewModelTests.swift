//
//  HomePresenterTests.swift
//  TheMovieDBTests
//
//  Created by Orlando Fortich on 31/07/21.
//

import Foundation
import XCTest
@testable import TheMovieDB

class HomePresenterTests: XCTestCase {

    func testSuccessFetchHome() {
        let apiClient = MockAPIClient()
        let Presenter = HomePresenter(apiClient: apiClient)
        Presenter.fetchHome()
        guard case FetchingServiceState.finishedLoading = Presenter.state.value else {
            XCTFail()
            return
        }
        let movie = Presenter.HomeList.value[0]
        let mockMovie = Movie.getMockMovie()
        XCTAssertEqual(movie.title, mockMovie.title)
        XCTAssertEqual(movie.id, mockMovie.id)
        XCTAssertEqual(movie.overview, mockMovie.overview)
        XCTAssertEqual(movie.rating, mockMovie.rating)
    }

    func testFailFetchHome() {
        let apiClient = MockAPIClient()
        apiClient.mockHomeResponse = .failure(NetworkError.unknown)
        let Presenter = HomePresenter(apiClient: apiClient)
        Presenter.fetchHome()
        guard case FetchingServiceState.error(_) = Presenter.state.value else {
            XCTFail()
            return
        }
    }
}

class MockAPIClient: APIClient {
    
    var mockHomeResponse: APIResponse<HomeResponse> = .success(HomeResponse.getMockHomeResponse())

    override func getHomeMovies(service: Service, completion: @escaping (APIResponse<HomeResponse>) -> ()) {
        return completion(mockHomeResponse)
    }
}
