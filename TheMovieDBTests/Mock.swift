//
//  Mock.swift
//  TheMovieDBTests
//
//  Created by Orlando Fortich on 31/07/21.
//

import Foundation
@testable import TheMovieDB

internal extension Movie {
    static func getMockMovie() -> Movie {
        return Movie(id: 1, title: "Titanic", overview: "Overview text", poster: "1.jpg", voteAverage: 8.7)
    }
}

internal extension HomeResponse {
    static func getMockHomeResponse () -> HomeResponse {
        return HomeResponse(movies: [Movie.getMockMovie()], totalPages: 1)
    }
}
