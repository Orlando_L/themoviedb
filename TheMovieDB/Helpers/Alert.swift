//
//  Alert.swift
//  TheMovieDB
//
//  Created by Orlando Fortich on 31/07/21.
//

import Foundation

struct AlertAction {
    let buttonTitle: String
    let handler: (() -> Void)?
}

struct SingleButtonAlert {
    let title: String
    let message: String?
    let action: AlertAction
}
