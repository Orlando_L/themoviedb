//
//  MovieDetailRouter.swift
//  TheMovieDB
//
//  Created by Orlando Fortich on 31/07/21.
//

import Foundation
import UIKit

class MovieDetailRouter: Router {

    var navigationController: UINavigationController
    var movie: Movie

    init(navigationController: UINavigationController, movie: Movie) {
        self.navigationController = navigationController
        self.navigationController.navigationBar.tintColor = UIColor.black
        self.movie = movie
    }

    func getViewController() -> UIViewController {
        return MovieDetailViewController(movie: movie)
    }

    func show(present: Bool = false) {
        let movieDetailViewController = getViewController()
        if present {
            movieDetailViewController.modalTransitionStyle = .crossDissolve
            self.navigationController.viewControllers.last?.present(movieDetailViewController, animated: true, completion: nil)
        } else {
            self.navigationController.navigationBar.prefersLargeTitles = true
            self.navigationController.pushViewController(movieDetailViewController, animated: true)
        }
    }
}
