//
//  HomeRouter.swift
//  TheMovieDB
//
//  Created by Orlando Fortich on 31/07/21.
//

import UIKit
import Foundation

protocol HomeRouterDelegate {
    func movieSelected(movie: Movie)
}

class HomeRouter: Router {

    var navigationController: UINavigationController
    var homePresenter = HomePresenter()
    var searchPresenter = SearchPresenter()
    var delegate: HomeRouterDelegate?

     init(navigationController: UINavigationController) {
         self.navigationController = navigationController
     }

    func getViewController() -> UIViewController {
        let viewController = HomeViewController(Presenter: homePresenter, searchPresenter: searchPresenter)
        viewController.RouterDelegate = self
        return viewController
    }

    func show(present: Bool = false) {
        let HomeViewController = getViewController()
        if present {
            HomeViewController.modalTransitionStyle = .crossDissolve
            self.navigationController.viewControllers.last?.present(HomeViewController, animated: true, completion: nil)
        } else {
            self.navigationController.navigationBar.prefersLargeTitles = true
            self.navigationController.pushViewController(HomeViewController, animated: true)
        }
    }
}

extension HomeRouter: HomeRouterDelegate {
    func movieSelected(movie: Movie) {
        MovieDetailRouter(navigationController: self.navigationController, movie: movie).show()
    }
}
