//
//  HomeResponse.swift
//  TheMovieDB
//
//  Created by Orlando Fortich on 31/07/21.
//

import Foundation

struct HomeResponse: Codable {
    var movies: [Movie]
    var totalPages: Int

    enum CodingKeys: String, CodingKey {
        case movies = "results"
        case totalPages = "total_pages"
    }
}
