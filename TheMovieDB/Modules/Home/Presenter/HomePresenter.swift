//
//  HomePresenter.swift
//  TheMovieDB
//
//  Created by Orlando Fortich on 31/07/21.
//

import Foundation

class HomePresenter {

    private (set) var state: Bindable<FetchingServiceState> = Bindable(.loading)
    private let apiClient: APIClient
    private var searchResponse: SearchResponse?
    private (set) var HomeList: Bindable<[Movie]> = Bindable([])
    private (set) var currentPage: Int = 1
    private (set) var totalPages: Int = Int.max

    init(apiClient: APIClient = APIClient()) {
        self.apiClient = apiClient
    }

    func fetchHome() {
        if currentPage > totalPages { return }
        state.value = .loading
        apiClient.getHomeMovies(service: HomeAPI(paramters: [NetworkConstants.pageParameterKey: "\(currentPage)"]), completion: { [weak self] response in
            self?.state.value = .finishedLoading
            switch response {
            case .success(let result):
                self?.HomeList.value.append(contentsOf: result.movies)
                self?.totalPages = result.totalPages
                self?.currentPage += 1
            case .failure(let error):
                self?.state.value = .error(error)
            }
        })
    }
}

