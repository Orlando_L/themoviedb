//
//  LoadingCell.swift
//  TheMovieDB
//
//  Created by Orlando Fortich on 31/07/21.
//

import UIKit

class LoadingCell: UITableViewCell {
    static let cellIdentifier = String(describing: LoadingCell.self)
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
}
