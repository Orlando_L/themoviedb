//
//  EmptyDataCell.swift
//  TheMovieDB
//
//  Created by Orlando Fortich on 31/07/21.
//

import UIKit

class EmptyDataCell: UITableViewCell {
    static let cellIdentifier = String(describing: EmptyDataCell.self)
    @IBOutlet weak var emptyMessageLabel: UILabel!
}
