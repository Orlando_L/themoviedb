//
//  MovieCell.swift
//  TheMovieDB
//
//  Created by Orlando Fortich on 31/07/21.
//

import UIKit
import Kingfisher

class MovieCell: UITableViewCell {

    static let cellIdentifier = String(describing: MovieCell.self)
    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
//    @IBOutlet weak var overviewLabel: UILabel!

    var movie: Movie? {
        didSet {
            titleLabel.text = movie?.title
//            overviewLabel.text = movie?.overview
            let placeholderImage = UIImage(named: "placeholder")
            if let moviePoster = movie?.posterUrl() {
//                print(movie?.posterUrl())
                posterImage.kf.setImage(with: moviePoster, placeholder: placeholderImage)
            } else {
                posterImage.image = placeholderImage
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        posterImage.layer.cornerRadius = 10
        self.selectionStyle = .none
    }
}
