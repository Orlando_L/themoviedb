//
//  AppDelegate.swift
//  TheMovieDB
//
//  Created by Orlando Fortich on 31/07/21.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var homeRouter: HomeRouter?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        homeRouter = HomeRouter(navigationController: UINavigationController())
        homeRouter?.show()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = homeRouter?.navigationController
        window?.makeKeyAndVisible()
        return true
    }
}
