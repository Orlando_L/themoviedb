//
//  NetworkConstants.swift
//  TheMovieDB
//
//  Created by Orlando Fortich on 31/07/21.
//

import Foundation

struct NetworkConstants {
    static let defaultRequestParams = ["api_key": "4b0c5009ab38ac89721d29ae23afb863"]
    static let defaultRequestHeaders = ["Content-type": "application/json; charset=utf-8"]
    static let baseURL = "https://api.themoviedb.org/3"
    static let HomeServicePath = "/movie/now_playing"
    static let searchServicePath = "/search/movie"
    static let imagesBaseURL = "https://image.tmdb.org/t/p/w500/"
    static let queryParameterKey = "query"
    static let pageParameterKey = "page"
}
