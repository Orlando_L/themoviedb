//
//  Typealias.swift
//  TheMovieDB
//
//  Created by Orlando Fortich on 31/07/21.
//

import Foundation

typealias RequestHeaders = [String: String]
typealias DataTaskResult = (Data?, URLResponse?, Error?) -> ()
