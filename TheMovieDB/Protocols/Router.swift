//
//  Router.swift
//  TheMovieDB
//
//  Created by Orlando Fortich on 31/07/21.
//

import UIKit

protocol Router {
    var navigationController: UINavigationController { get set }
    func getViewController() -> UIViewController
    func show(present: Bool)
}
